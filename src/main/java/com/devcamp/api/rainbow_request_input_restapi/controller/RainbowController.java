package com.devcamp.api.rainbow_request_input_restapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.rainbow_request_input_restapi.service.RainbowService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@CrossOrigin
public class RainbowController {
    @Autowired
    RainbowService rainbowService;

    @GetMapping("/rainbow-request-query")
    public ArrayList<String> searchRainbow(@RequestParam(value = "q", defaultValue = "") String keyword) {
        //RainbowService rainbowService = new RainbowService();

        return rainbowService.searchRainbow(keyword);        
    }    

    @GetMapping("/rainbow-request-param/{ind}")
    public String getMethodName(@PathVariable(name="ind") int index) {
        return rainbowService.getRainbow(index);
    }    
}
