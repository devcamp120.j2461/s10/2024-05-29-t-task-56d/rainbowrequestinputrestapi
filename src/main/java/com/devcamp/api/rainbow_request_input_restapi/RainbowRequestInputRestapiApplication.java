package com.devcamp.api.rainbow_request_input_restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowRequestInputRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbowRequestInputRestapiApplication.class, args);
	}

}
